const { join } = require('path')

module.exports = {
  productionBrowserSourceMaps: true,
  i18n: {
    locales: ['en'],
    defaultLocale: 'en'
  },
  sassOptions: {
    includePaths: [join(__dirname, 'styles')],
    prependData: ` // Add sass globals here ↓
      @import 'constants';
    `
  },
}
