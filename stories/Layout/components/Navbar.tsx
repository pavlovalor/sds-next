import { forwardRef, useState, useLayoutEffect, useCallback, useContext, useRef } from "react"
import SbEditable from "storyblok-react"
import styles from "../styles.module.scss"
import Link from "next/link"
import clsx from "clsx"
import { Wrapper, Logo } from "components"
import { Context } from ".."

import type { NavigationProps, ItemProps } from "../types"
import type { FC } from "react"


/** Renders floating navigation menu */
export const Navbar = forwardRef<HTMLDivElement, NavigationProps>((
  { children, blok, className, ...rest }, ref,
) => {
  const menu = useRef<HTMLUListElement>(null)
  const [isOpen, setOpenState] = useState(false)
  const [scrollValue, setScrollValue] = useState(0)
  const [caretPosition, setCaretPosition] = useState({ left: 0, width: 0 })
  const { navbarHeight } = useContext(Context)
  const top = (navbarHeight - scrollValue) * .5
  const containerStyles = clsx(styles.navigation, className, {
    [styles.fixed]: !top,
    [styles.open]: isOpen,
  })

  /** Tracks if windows is scroll in range of 0 and `navbarHeight` */
  const scrollHandler = useCallback(({ target } = {}) => {
    const trackValue = !target || target === document ? window.scrollY : 0
    const safeValue = trackValue > navbarHeight ? navbarHeight : trackValue
    if (safeValue !== scrollValue) setScrollValue(safeValue)
  }, [navbarHeight, scrollValue, setScrollValue])

  /** Computes highlight carret position */
  const mouseHandler = useCallback(({ target, type } = {}) => {
    const isLeaveEvent = type !== 'mouseenter'
    const query = `.${styles.item}${isLeaveEvent ? ` a[href="${location.pathname}"]` : ''}`
    const { offsetLeft = 0, offsetWidth = 0 } = (isLeaveEvent 
      ? menu.current.querySelector(query) : target) ?? {}
    if (caretPosition.left !== offsetLeft && caretPosition.width !== offsetWidth)
      setCaretPosition({ left: offsetLeft, width: offsetWidth })
  }, [caretPosition, setCaretPosition])

  useLayoutEffect(() => { /* Eventlisteners setup */
    mouseHandler()
    scrollHandler()
    window.addEventListener('scroll', scrollHandler, { passive: true })
    window.addEventListener('resize', mouseHandler, { passive: true })
    return () => {
      window.removeEventListener('scroll', scrollHandler)
      window.removeEventListener('scroll', mouseHandler)
    }
  }, [scrollHandler])

  return (
    <SbEditable content={blok}>
      <nav itemScope 
        itemType="http://schema.org/SiteNavigationElement"
        className={containerStyles} 
        ref={ref} {...rest}>
        <div className={styles.bar}>
          <Wrapper className={styles.wrapper}>
            <Logo className={styles.logo} 
              size={80} mode={top || isOpen ? 'white' : 'color'}/>
            <ul role="menubar" className={styles.menu} ref={menu}>
              {blok.menu.map((item, index) => <NavItem key={item._uid}
                style={{ transitionDelay: index / 20 + 's' }}
                onMouseLeave={mouseHandler}
                onMouseEnter={mouseHandler}
                blok={item} 
              /> )}
              <div className={styles.highlighter} style={{ 
                transform: `translateX(${caretPosition.left}px)`,
                width: caretPosition.width, 
              }} />
            </ul>
            <button className={styles.hamburger} onClick={setOpenState.bind(null, !isOpen)}>
              <span/><span/><span/>
            </button>
          </Wrapper>
        </div>
      </nav>
    </SbEditable>
  )
})


export const NavItem: FC<ItemProps> = ({ blok, ...rest }) => {
  const isExternal = blok.reference?.linktype === 'url'
  return (
    <SbEditable content={blok}>
      <li itemProp="name" className={styles.item} {...rest}>
        {isExternal ? (
          <a itemProp="url" href={blok.reference?.url}>
            {blok.text}
          </a>
        ) : (
          <Link href={blok.reference?.url ?? '#'}>
            <a itemProp="url">
              {blok.text}
            </a>
          </Link>
        )}
      </li>
    </SbEditable>
  )
}
