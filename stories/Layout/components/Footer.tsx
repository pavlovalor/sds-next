
import { forwardRef } from "react"
import { Wrapper } from "components"
import styles from "../styles.module.scss"
import SbEditable from "storyblok-react"
import * as Icons from "react-icons/ai"
import Link from "next/link"
import clsx from "clsx"

import type { FooterProps, ItemProps } from "../types"
import type { FC } from "react"


export const Footer = forwardRef<HTMLDivElement, FooterProps>(({ blok }, ref) => (
  <SbEditable content={blok}>
    <footer className={clsx(styles.footer)} ref={ref}>
      <Wrapper className={styles.wrapper}>
        <ul className={styles.menu} role="menubar"
         itemScope itemType="https://schema.org/SiteNavigationElement">
          <li>{blok.copyright.replace('{{year}}', new Date().getFullYear().toString())}</li>
          {blok.menu.map(item => <LinkItem blok={item} key={item._uid} />)}
        </ul>
        <ul className={styles.menu} role="menubar">
          {blok.socials.map(item => <LinkItem blok={item} key={item._uid} />)}
        </ul>
      </Wrapper>
    </footer>
  </SbEditable>
))

export const LinkItem: FC<ItemProps> = ({ blok, ...rest }) => {
  const Icon = Icons[blok.icon] ?? 'span'
  const isExternal = blok.reference?.linktype === 'url'
  return (
    <SbEditable content={blok}>
      <li itemProp="name" {...rest}>
        {isExternal ? (
          <a itemProp="url" href={blok.reference?.url}>
            <Icon className={styles.icon} /> {blok.text}
          </a>
        ) : (
          <Link href={blok.reference?.url ?? '#'}>
            <a itemProp="url">
              <Icon className={styles.icon} /> {blok.text}
            </a>
          </Link>
        )}
      </li>
    </SbEditable>
  )
}
