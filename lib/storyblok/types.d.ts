export declare namespace Storyblok {
  interface LinkField {
    cached_url: string
    fieldtype: string
    linktype: 'url' | 'story'
    url: string
    id: string
  }

  interface StoryData<C extends any> {
    uuid: string
    name: string
    alternates: [] // ???
    content: C
    created_at: string
    published_at: string
    first_published_at: string
    group_id: string
    default_full_slug: string | null
    id: number
    is_startpage: boolean
    lang: string
    parent_id: number
    release_id: number | null
    path: string | null
    position: number
    slug: string
    sort_by_date: null // ???
    tag_list: [] // ???
    translated_slugs: [] // ???
  }

}




export interface BlokProps<P = any> {
  blok: P
}

export interface LinkItem {
  id: string
  uid: string
  slug: string
  name: string
  is_startpage: boolean
  is_folder: boolean
  published: boolean
  parent_id: number
  position: number
  real_path: string
}

export interface EntryProps {
  
}
