import StoryblokClient from "storyblok-js-client"
import { accessToken } from "./config"

/**
 * Connects NextJS app with Storyblok CMS
 * IMPORTANT: Do not store access token in code file!
 * Use .env files and secret keepers
 */
export default new StoryblokClient({
  cache: { clear: 'auto', type: 'memory' },
  accessToken,
})
