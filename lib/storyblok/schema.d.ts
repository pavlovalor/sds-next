import type { SbEditableContent } from "storyblok-react"

export interface NavigationItem extends SbEditableContent {
  icon ?: any,
  nofollow: boolean
  target_blank: boolean
  reference: any
  text: string
  title: string
}
