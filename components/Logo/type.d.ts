import type { FC, HTMLAttributes } from "react"

interface Props extends HTMLAttributes<SVGElement> {
  mode?: 'color' | 'flat' | 'white' | 'black'
  details?: 'full' | 'minimalistic'
  size: number
}

export type Component = FC<Props>
