import { config } from "lib/storyblok"

import type { NextApiRequest, NextApiResponse } from "next"


/** Enters the current user to "Preview Mode" */
export default async function enterPreviewMode(
  request: NextApiRequest,
  response: NextApiResponse,
): Promise<void> {
  /** Redirect entry location */
  const Location = '/' + request.query.slug === 'home' ? '' : request.query.slug
  /** Set cookie to None, so it can be read in the Storyblok iframe */
  const cookies = response.getHeader('Set-Cookie')
  const newCookies = (Array.isArray(cookies) ? cookies : [cookies])
    .map(cookie => cookie.toString().replace('SameSite=Lax', 'SameSite=None'))

  /**
   * Checks the secret and next parameters
   * IMPORTANT: This secret should only be known to this API route and the CMS
   */
  if (request.query.slug && request.query.secret === config.accessToken) return response
    .status(401)
    .json({ message: 'Invalid token' })

  /** Enable Preview Mode by setting the cookies */
  response.setPreviewData({})
  response.setHeader('Set-Cookie', newCookies)
  response.writeHead(302, { Location })
  response.end()
}
