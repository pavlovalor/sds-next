import { storyblokClient, StoryEntry } from "lib/storyblok"

import type { GetStaticPaths, GetStaticProps } from "next"
import type { LinkItem } from "lib/storyblok/types"


const IS_DELOPMENT = process.env.NODE_ENV === 'development'
const DEFAULT_SLUG = 'home'


/**
 * Generates list with all available URLs
 * @see https://bit.ly/3baSPku NextJS Documentation
 * 
 * TODO: review later
 */
export const getStaticPaths: GetStaticPaths = async ({ 
  /*locales,*/ defaultLocale
}) => {
  const { data } = await storyblokClient.get('cdn/links/', { 
    version: IS_DELOPMENT ? 'draft' : 'published'
  })
  return {
    fallback: IS_DELOPMENT, /** Prevent 404 error for dev env */
    paths: Object.entries(data.links as LinkItem[])
      .filter(([_, { is_folder }]) => !is_folder)
      .map(([_, { slug }]) => ({
        locale: defaultLocale,
        params: { slug: slug === DEFAULT_SLUG ? [] : [ slug ] },
      })),
  }
}

/** 
 * Generates props for `StoryEntry` depending on URL
 * @see https://bit.ly/3uKZZDE NextJS Documentation
 * 
 * TODO: review locales usage
 */
export const getStaticProps: GetStaticProps = async ({
  params: { slug: rawSlug = [ DEFAULT_SLUG ], ...params },
  preview, /** Indicates if page was requested using NextJS preview mode */
  // locales, 
}) => {
  /** Combines slug path */
  const slug = (rawSlug as string[]).join('/')
  /** Loads the story from the Storyblok API */
  let { data } = await storyblokClient.get(`cdn/stories/${slug}`, {
    /** Resolve parent components. Global layout for example */
    resolve_relations: 'parent', 
    /** Search for draft version in preview or published for production */
    version: preview ? 'draft' : 'draft',//: 'published',
    /** Appends the cache version to get the latest content */
    cv: Date.now(),
    /** Forward rest params including those needed for visual composer */
    ...params,
  })
  return { 
    props: { /** This will be available in `StoryEntry` */
      story: data ? data.story : false,
      preview: preview ?? false
    },
    /** Defines when page will be expaired */
    revalidate: 10, 
  }
}

export default StoryEntry
